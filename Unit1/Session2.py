# This is free software, subject to the license terms of the LICENSE - please see the root of the repo for more info.

FloatN1:float = 3.14
FloatN2:float = 5.5
Result = FloatN1 + FloatN2
print("----------")
print(f"Sum of two floating point numbers ({FloatN1} and {FloatN2}): {Result}")
print(f"The type of the result is: {type(Result)}")
print("----------")

IntN1:int = 3
IntN2:int = 5
Result = IntN1 + IntN2
print(f"Difference between two integers ({IntN1} and {IntN2}): {Result}")
print(f"The type of the result is: {type(Result)}")
print("----------")

IntN1:int = 3
FloatN2:int = 5.5
Result = IntN1 * FloatN2
print(f"Product between an int and a float ({IntN1} and {FloatN2}): {Result}")
print(f"The type of the result is: {type(Result)}")
print("----------")