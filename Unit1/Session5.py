# This is free software, subject to the license terms of the LICENSE - please see the root of the repo for more info.
import math


def PrintTable():

    StartX = 0
    EndX = 2*math.pi
    NumSteps = 1000

    # Calc Step
    StepSize = (EndX - StartX) / NumSteps

    # Now, Generate Table
    print("-------------------------------------------")
    print("X                    | sin(x)     ")
    print("-------------------------------------------")
    for x in range(NumSteps):
        print(f"{str(StepSize*x).ljust(20)} | {math.sin(StepSize*x)}")

if __name__ == "__main__":
    PrintTable()