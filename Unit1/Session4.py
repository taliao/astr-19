# This is free software, subject to the license terms of the LICENSE - please see the root of the repo for more info.

class Penguin: # Obviously have to do the penguin because Linux is the superior language

    def __init__(self):

        self.NumEyes_:int = 2
        self.ArmLength_cm:float = 41.
        self.LegLength_cm:float = 0. # they technically have internal legs apparently
        self.HasTail:bool = True
        self.IsFurry:bool = True

    def PrintInfo(self):

        print("Name: Penguin")
        print(f"Number of eyes: {self.NumEyes_}")
        print(f"Arm Length (in centimeters): {self.ArmLength_cm}")
        print(f"Leg Length (in centimeters): {self.LegLength_cm} (they have internal legs, so technically 0)")
        print(f"Has Tail: {self.HasTail}")
        print(f"Is Furry: {self.IsFurry}")
        

if __name__ == "__main__":
    P = Penguin()
    P.PrintInfo()