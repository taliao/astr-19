# This is free software, subject to the license terms of the LICENSE - please see the root of the repo for more info.

def f(x):
    return x**3 + 8


print(f"Calling f(x) with x = 9: {f(9)}")
if (f(9) > 27):
    print("YAY!")